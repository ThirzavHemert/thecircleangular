import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private url = 'https://truyouapi.herokuapp.com';
  private socket;

  constructor() {
    this.socket = io(this.url);
   }

   public sendMessage(message,user,room){
     console.log('Service sendMessages called', message)
     this.socket.emit('messagedetection', message, user, "","",room);
  }

  joinRoom(data)
    {
      console.log('Joinroom', data.user, data.room);
      this.socket.emit('join', data.user, data.room);
    }

    newUserJoined()
    {
        let observable = new Observable<{user:String, message:String}>(observer=>{
            this.socket.on('new user joined', (data)=>{
                observer.next(data);
            });
            return () => {this.socket.disconnect();}
        });

        return observable;
    }

    public getMessages = () => {
        let observable = new Observable<{senderNickname:String, message:String}>(observer=>{
            this.socket.on('message', (data,data2)=>{
                console.log(data.senderNickname+ "::::"+data.message)

                let a={
                  senderNickname:data.senderNickname,
                  message:data.message
                }
                observer.next(a);
            });
            return () => {this.socket.disconnect();}
        });

        return observable;
    }

  // public getMessages = () => {
  //   console.log('Service getMessages called')
  //   return Observable.create((observer) => {
  //         console.log("Service in observable")
  //         this.socket.on('goMessage', (message) => {
  //             console.log("messgae: ", message)
  //             observer.next(message);
  //           });
  //       });
  //   }
}
